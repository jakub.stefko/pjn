import re

lineRegex = re.compile(r'^[a-zA-Z]*\;[0-9]*\;[0-9]*\;$')

def main():
  with open('./sth.csv') as file:
    line = file.readline()
    counter = 1
    isFileCorrect = True

    while line:
      if not lineRegex.match(line):
        print("Line {}: contains error".format(counter))
        if isFileCorrect: isFileCorrect = False
      line = file.readline()
      counter += 1

  if (isFileCorrect): print("Everything all right")
  file.close()

main()
