import re

def main():
  with open('./bad.txt') as bad:
    line = bad.readline()

    while line:
      line = re.sub('Chuj', '---', line)
      line = re.sub('Chuja', '---', line)
      line = re.sub('Chujom', '---', line)
      line = re.sub('Chuju', '---', line)
      line = re.sub('Chujowi', '---', line)

      line = re.sub('chuj', '---', line)
      line = re.sub('chuja', '---', line)
      line = re.sub('chujom', '---', line)
      line = re.sub('chuju', '---', line)
      line = re.sub('chujowi', '---', line)
      
      line = re.sub('Kurwa', '---', line)
      line = re.sub('Kurwo', '---', line)
      line = re.sub('Kurwie', '---', line)
      line = re.sub('Kurwiu', '---', line)

      line = re.sub('kurwa', '---', line)
      line = re.sub('kurwo', '---', line)
      line = re.sub('kurwie', '---', line)
      line = re.sub('kurwiu', '---', line)

      line = re.sub('Dupa', '---', line)
      line = re.sub('Dupy', '---', line)
      line = re.sub('Dupie', '---', line)
      line = re.sub('Dupom', '---', line)
      line = re.sub('Dupo', '---', line)

      line = re.sub('dupa', '---', line)
      line = re.sub('dupy', '---', line)
      line = re.sub('dupie', '---', line)
      line = re.sub('dupom', '---', line)
      line = re.sub('dupo', '---', line)
      
      print(line)
      line = bad.readline()

  bad.close()


main()