import re

properNounRegex = re.compile(r'^[A-Z][a-zA-Z0-9]*$')
telephoneRegex = re.compile(r'^\([0-9]{2}\) [0-9]{3}\-[0-9]{2}\-[0-9]{2}$')
postCodeRegex = re.compile(r'^[0-9]{2}\-[0-9]{3}$')

def isProperNoun(string): return properNounRegex.match(string)
def isTelephone(string): return telephoneRegex.match(string)
def isPostCode(string): return postCodeRegex.match(string)

def main():
  name = input("Enter your name: ")
  surname = input("Enter your surname: ")
  telephone = input("Enter your phone: ")
  postCode = input("Enter your post code: ")
  city = input("Enter your city: ")
  
  if not isProperNoun(name): print("Sorry, name must obtain proper noun regex: \'^[A-Z][a-zA-Z0-9]*$\'")
  if not isProperNoun(surname): print("Sorry, surname must obtain proper noun regex: \'^[A-Z][a-zA-Z0-9]*$\'")
  if not isTelephone(telephone): print("Sorry, telephone number must obtain regex: \'^\([0-9]{2}\) [0-9]{3}\-[0-9]{2}\-[0-9]{2}$\'")
  if not isPostCode(postCode): print("Sorry, post code must obtain regex: \'^[0-9]{2}\-[0-9]{3}$\'")
  if not isProperNoun(city): print("Sorry, city name must obtain proper noun regex: \'^[A-Z][a-zA-Z0-9]*$\'")

main()
