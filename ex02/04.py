import re

mailRegex = re.compile(r'[a-zA-z.0-9]+\@[a-zA-Z0-9.]+')

def main():
  with open('./page.txt') as file:
    line = file.readline()
    mailList = []

    while line:
      check = re.findall(mailRegex, line)
      for i in check: mailList.append(i)
      line = file.readline()

  print('Response: ', mailList)
  file.close()

main()
