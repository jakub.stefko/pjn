Rafał Jaworski: Home Strona główna UAM
Strona WMiI
RAFAŁ JAWORSKI - STRONA DOMOWA STRONA GŁÓWNA DYDAKTYKA PRACA NAUKOWA CZAS WOLNY KONTAKT aktualności 01.09.2015 Udostępniono interaktywne demo systemu Concordia.
Więcej szczegółów na: link.
03.06.2015 Kolejny wpis w dziale "czas wolny".
10.05.2015 Nowy wpis w dziale "czas wolny".
28.04.2015 Opublikowano pierwszą wersję programu Concordia.
24.04.2015 Dodano angielską wersję strony.
21.04.2015 Zmiana zdjęcia.
07.11.2013 Dodanie działu "listy od studentów" w "czas wolny".
04.10.2013 Aktualizacja strony na semestr zimowy 2013/2014.
29.03.2011 Aktualizacja strony idodanie działu Publikacje.
02.10.2009 Aktualizacja strony na semestr zimowy 2010/2011.
25.09.2009 Premiera strony.
dodatkowe linki System EduWiki kontakt dr Rafał Jaworski, rjawor at amu.edu.pl o mnie Nazywam się Rafał Jaworski i witam na mojej stronie domowej.
Najpierw kilka słów o mnie.
Jestem absolwentem Wydziału Matematyki i Informatyki Uniwersytetu im.
Adama Mickiewicza oraz Studium Doktoranckiego tego wydziału.
Posiadam stopień doktora nauk matematycznych w zakresie informatyki.
Obecnie jestem pracownikiem Wydziału na stanowisku adiunkta.
Prowadzę zarówno pracę naukową, jak i dydaktyczną.
Materiały do prowadzonych przeze mnie zajęć znajdują się w dziale "Dydaktyka".
W mojej pracy naukowej łączę informatykę z inną pasją - lingwistyką (mówię po angielsku, hiszpańsku, niemiecku, a także trochę po włosku).
Zajmuję się zagadnieniami lingwistyki komputerowej, opisanymi bardziej szczegółowo w dziale "Praca naukowa".
Jestem twórcą projektu Concordia - wyszukiwarki pełnotekstowej, przeznaczonej dla komputerowego wspomagania tłumaczenia.
Zapraszam na stronę projektu: http://tmconcordia.sourceforge.net/ Posiadam również wiele zainteresowań pozanaukowych.
Można zapoznać się z nimi w dziale "Czas wolny".
W dziale "Kontakt" umieściłem natomiast moje informacje kontaktowe.
Zapraszam do przeglądania strony! © 2018 Rafał Jaworski
rjawor at amu.edu.pl
XHTML 1.1
CSS
design by dcarter 