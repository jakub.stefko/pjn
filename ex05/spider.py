import scrapy

def save_page(page_name: str, page_data):
  file = open('./response/' + page_name + '.txt', 'a')
  file.write(page_data)
  file.close()

class RafalJaworskiSpider(scrapy.Spider):
  name = 'rafaljaworskispider'
  start_urls = ['http://rjawor.home.amu.edu.pl/index.php', 'http://rjawor.home.amu.edu.pl/index_en.php']
  unique_data = []

  def parse(self, response):
    if response.url not in self.unique_data:
      self.unique_data.append(response.url)

      save_page(response.url[30 : len(response.url) - 4], response.text)

      for next_page_link in response.css('#menu ul li a').xpath('@href').getall():
        href = 'http://rjawor.home.amu.edu.pl/' + next_page_link
        if href not in self.unique_data:
           yield response.follow(href, self.parse)
