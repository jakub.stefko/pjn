<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
	<title>Rafał Jaworski: Home</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="style/style106_left.css" />
	<link rel="stylesheet" type="text/css" href="style/colour8.css" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://bibtex-js.googlecode.com/svn/trunk/src/bibtex_js.js"></script>
</head>

				
<body>
	<div id="main">
				<div id="links">
			<a href="http://amu.edu.pl" target="blank">Strona główna UAM</a> | <a href="http://www.wmi.amu.edu.pl/" target="blank">Strona WMiI</a> | <a href="index.php" title="Wersja polska"><img width="3%" src="pl.png"/></a> | <a href="index_en.php" title="English version"><img  width="3%" src="uk.png"/></a>
		</div>

				
				<div id="logo"><h1>RAFAŁ JAWORSKI - STRONA DOMOWA</h1></div>

				
		<div id="content">
			<div id="menu">
				<ul>
					<li><a href="index.php">STRONA GŁÓWNA</a></li>
					<li><a href="education.php">DYDAKTYKA</a></li>
					<li><a href="science.php">PRACA NAUKOWA</a></li>
					<li><a id="selected" href="relax.php">CZAS WOLNY</a></li>
					<li><a class="last" href="contact.php">KONTAKT</a></li>
				</ul>
			</div>
			<div id="column1">
				        <div class="sidebaritem">
          <h1>aktualności</h1>
          <h2>01.09.2015</h2>
          <p style="text-align:left">Udostępniono interaktywne demo systemu Concordia. Więcej szczegółów na: <a href="http://tmconcordia.sourceforge.net/demo.html" target="_blank">link</a>.</p>
          <h2>03.06.2015</h2>
          <p style="text-align:left">Kolejny wpis w dziale "czas wolny".</p>
          <h2>10.05.2015</h2>
          <p style="text-align:left">Nowy wpis w dziale "czas wolny".</p>
          <h2>28.04.2015</h2>
          <p style="text-align:left">Opublikowano pierwszą wersję programu <a href="http://tmconcordia.sourceforge.net/" target="_blank">Concordia.</a></p>
          <h2>24.04.2015</h2>
          <p style="text-align:left">Dodano angielską wersję strony.</p>
          <h2>21.04.2015</h2>
          <p style="text-align:left">Zmiana zdjęcia.</p>
          <h2>07.11.2013</h2>
          <p style="text-align:left">Dodanie działu "listy od studentów" w "czas wolny".</p>
          <h2>04.10.2013</h2>
          <p style="text-align:left">Aktualizacja strony na semestr zimowy 2013/2014.</p>
          <h2>29.03.2011</h2>
          <p style="text-align:left">Aktualizacja strony i&nbsp;dodanie działu Publikacje.</p>
          <h2>02.10.2009</h2>
          <p style="text-align:left">Aktualizacja strony na semestr zimowy 2010/2011.</p>
          <h2>25.09.2009</h2>
          <p>Premiera strony.</p>
        </div>

				
								<div class="sidebaritem">
					<h1>dodatkowe linki</h1>
					<a href="http://eduwiki.wmid.amu.edu.pl" target="blank">System EduWiki</a>
				</div>

				
								<div class="sidebaritem" style="text-align:left">
					<h1>kontakt</h1>
					<p>
						dr Rafał Jaworski,<br /> rjawor at amu.edu.pl
					</p>
				</div>

				
			</div>
			<div id="column2">
				<!--PAGE CONTENT -->
				<h1>czas wolny</h1>
				<p>
				Informatyka nie zajmuje całego mojego czasu - mam dużo zainteresowań pozanaukowych. Największe to:
				<ul>
					<li>
					Nauka języków obcych - robię to dla czystej przyjemności rozmawiania z cudzoziemcami w ich własnym języku.
					</li>
					<li>
					Muzyka - gra na gitarze (akustycznej, dwunastostrunowej) oraz słuchanie starego, dobrego rocka (ulubione zespoły: Led Zeppelin i Lady Pank).
					</li>
					<li>
					Motoryzacja - kolekcjonowanie wszelkiej możliwej wiedzy na temat samochodów.
					</li>
					<li>
					Sport - jeżdżenie na rowerze (szosowym i górskim), bieganie, akrobatyka sportowa.
					</li>
					<li>
					Stare gry telewizyjne - mam SNES (Super Nintendo Entertainment System) oraz prawdziwą legendę - PEGASUSa (potrafię m.in. przejść Contrę na jednym życiu, oraz całe Super Mario, bez używania skrótów ☺).
					</li>
				</ul>
				</p>
				<h1 id="opowiadanie">opowiadanie</h1>
				<p>
				Przez pewien bardzo krótki okres mojego życia byłem również powieściopisarzem. W czasach szkoły średniej popełniłem opowiadanie p.t. "Sztuka latania". Było ono inspirowane stylem niestety nieżyjącego już Mistrza Sławomira Mrożka. Polecam lekturę jego prozy, choćby opowiadań "Ptaszek ugupu" oraz "Jak walczyłem" i wielu innych (natomiast obecny w kanonie lektur dramat "Tango" można, moim zdaniem, spokojnie pominąć). I choć mój tekst z pewnością nie dorasta do pięt dziełom Mistrza, publikuję go w nadziei, że choć jednemu czytelnikowi wyda się ciekawy: <a href="story.php">"Sztuka latania"</a>.
				</p>
				<h1 id="historia">film z Wydziału Historii</h1>
                                <p>
                                   Przy moim współudziale powstał filmik o budynku Wydziału Historii. Jest on dostępny na portalu YouTube: <a href="https://youtu.be/XmDrwOEp5cs" target="blank">link</a>.
                                </p>
				<h1 id="listy">listy od studentów</h1>
				<p>
				    Ostatnio pochyliłem się nad ciekawą kwestią. Jest nią forma wiadomości e-mail, które otrzymuję od studentów. Jako osoba żywo zainteresowana kulturą języka polskiego i kulturą w ogóle, postanowiłem w tym dziale umieścić garść przemyśleń na temat korespondencji elektronicznej. 
				</p>
				<p>
				    Pierwszym miejscem, które może, delikatnie mówiąc, zaskoczyć wykładowcę - odbiorcę wiadomości, jest pole "OD". Na naszym Wydziale przyjęło się używać poczty wydziałowej, która została stworzona do komunikacji z wykładowcami. Gdy jednak nie ma możliwości jej użycia, warto zatroszczyć się, aby prywatny adres e-mailowy nie zdradzał pseudonimu, zainteresowań i boję się myśleć, czego jeszcze. Adresy w rodzaju "chudy666@buziaczek.pl" powinny raczej służyć do komunikowania się ze znajomymi.
				</p>
				<p>
				    Kolejnym ważnym elementem każdego listu elektronicznego jest pole "Temat". Wbrew pozorom nie jest ono bezużyteczne, a jego wypełnienie nie powinno być przykrą koniecznością narzuconą przez program pocztowy. Temat nie powinien być streszczeniem listu, ale raczej wskazaniem jego głównej idei. Co do formy, polecam krótką frazę rzeczownikową, np. "zadanie domowe z programowania", "termin egzaminu" lub "problem z zadaniem 3". Tak napisany temat pozwala zorientować się, czego dotyczy wiadomość nawet bez wgłębiania się w jej treść, co jest niezwykle użyteczne w sytuacji, kiedy skrzynka odbiorcza wykładowcy pęka w szwach. Nie polecam zatem wpisywania tematów zbyt krótkich i ogólnych, takich jak "pytanie", czy "zajęcia". A w szczególności odradzam zostawianie pola "Temat" pustym. 
				</p>
				<p>
				I tak dochodzimy do meritum, czyli treści listu elektronicznego. Zwróćmy szczególną uwagę na słowo "list". E-mail jest odmianą listu, stąd forma e-maila powinna naśladować formę klasycznego listu. Dla przypomnienia: po pierwsze, list zaczynamy od zwrotu grzecznościowego do adresata. Zwrotem takim mogą być: "Szanowna Pani/Szanowny Panie", "Szanowna Pani Doktor/Szanowny Panie Doktorze", "Szanowna Pani Profesor/Szanowny Panie Profesorze". Zwrotami grzecznościowymi nie są: "cześć" oraz "witam" (ani nawet "witam serdecznie"). Po drugie, w pierwszych słowach listu elektronicznego należy przedstawić się wraz z podaniem jednostki organizacyjnej, do której się należy. Zastępuje to klasyczne pole "Nadawca", stosowane w listach, w którym oprócz imienia i nazwiska podawało się swój adres. W warunkach uczelnianych najlepiej przedstawić się wraz z podaniem grupy zajęciowej, np. "Nazywam się Jan Kowalski, uczestniczę w zajęciach z programowania w grupie 3". Po trzecie - zasadnicza treść. Powinna być zwięzła i rzeczowa, pozbawiona niepotrzebnych szczegółów i informacji pobocznych. Gorąco polecam przeczytanie treści swojego e-maila przed wysłaniem. I ostatnia rzecz - podpis poprzedzony słowami "z wyrazami szacunku" lub "z poważaniem" (a nie "pozdrawiam").
				</p>
				<p>
				Być może powyższe wytyczne są Państwu doskonale znane. Nie mogę jednak oprzeć się wrażeniu, że nie wszyscy się do nich stosują. Ku przestrodze, publikuję poniżej anonimowo najciekawsze sposoby łamania reguł pisania listów (zachowując pisownię oryginalną). Jeśli nie chcą Państwo trafić na tę tablicę, radzę pisać prawidłowo! 
				</p>
				<hr/>
				<h4>Temat:</h4>
				Zajęcia
				<h4>Treść:</h4>
				Mam takie pytanie czy jeżeli jutro by mnie nie było na ćwiczeniach to czy coś by się stało bo nie wiem jak jest z nieobecnościami, a bardzo mi nie pasuje jutrzejsza lekcja bo mam wyjazd prawie 600 km i nie chciałbym wyjechać o godzinie 19 po skończonych zajęciach, ponieważ to bardzo późno i stąd moje pytanie czy coś by się stało jakby mnie jutro nie było?
				<hr/>
				<h4>Temat:</h4>
				[Imię i nazwisko nadawcy]
				<h4>Treść:</h4>
				Witam serdecznie.<br/><br/>
W załączniku zadania.<br/><br/>
Pozdrawiam<br/>
[Imię i nazwisko nadawcy]
				<hr/>
				<h4>Temat:</h4>
				Zasadnicze pytanie
				<h4>Treść:</h4>
				Mam do Pana zasadnicze pytanie mianowicie gdy po wpisaniu do sprawdzarki swojego kodu otrzymuję odpowiedź "zła odpowiedź" znaczy że program daje złe wyniki czy można go lepiej napisać ? Czy to oznacza że jest tylko jedna poprawna odpowiedź ? Pozdrawiam [zdrobnienie imienia i nazwisko nadawcy]
                <hr/>
   				<h4>Od:</h4>
   				[zdrobnienie imienia i nazwisko, mail zdradzający pseudonim]
   				<h4>Temat:</h4>
				witam,
				<h4>Treść:</h4>
				Panie dr. ;) czy może Pan albo zmienić na [adres strony przedmiotu]
załączniki, tzn są one w rozszerzeniu *odp czyli zapewne OpenOffice lecz ja go nie posiadam, oczywiście mogę zainstalować i zrobię to jeżeli nie uda mi się Pana namówić na zmiane rozszerzeń tych plików na *ppt, albo przesłać mi pliki w *ppt.<br/>
-- <br/>
Pozdrawiam!
                <hr/>
                <b>I jeszcze prawdziwa perełka (bez cenzury):</b>
                <hr/>
   				<h4>Temat:</h4>
				Zajefajny domek w Blenderze [Blender - program graficzny]
				<h4>Treść:</h4>
				Witam przesyłam haupe w blenderze. Pozdrawim <br/><br/>
<iframe style="height:68px;width:100%" src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.staff.amu.edu.pl/~rjawor/relax.php#listy&amp;layout=button_count&amp;show_faces=true&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=80" scrolling="no" frameborder="0" allowTransparency="true"><style>.data-connect-fb{z-index:0;position:relative;font-color:#333; font-size:12;}</style><a id="fb-data-button" href="http://www.like-button.net"class="data-connect-fb">add facebook like button</a> <a href="http://www.maps-einbinden.net">google maps einbinden</a><style>.fbook-style_map:initreaction=10false_attempt10-border</style></iframe>
			</div>
		</div>
				<div id="footer">
			&copy 2018 Rafał Jaworski | <a href="mailto:rjawor@amu.edu.pl">rjawor at amu.edu.pl</a> | <a href="http://validator.w3.org/check?uri=referer">XHTML 1.1</a> | <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> | <a target="blank" href="http://www.dcarter.co.uk">design by dcarter</a>
		</div>

	</div>
</body>
</html>
