import csv
import tweepy


consumer_key = 'dRCyZO2oOEMdxKejgMFK3fyQD'
consumer_secret = 'c3o2q5neUJJFiLLn5OVnUJ9PP7ySLk8Ab7dcq5Q7qOGonlEg7I'
access_key = '1201428588123361280-t0MDRceF7d6KQzYYqdAkeXI8ytcQtO'
access_secret = '0KhSIVSBLqqYa3Vidrri2PkUcMqgjkOEJTnB1C4lXFxlo'


auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_key, access_secret)
api = tweepy.API(auth)
tweets_for_csv = []
number_of_tweets = 10


def get_tweets_from_user(username):
    for tweet in tweepy.Cursor(api.user_timeline, screen_name=username, tweet_mode="extended").items(number_of_tweets):
        if 'retweeted_status' in tweet._json:
            continue
        else:
            tweets_for_csv.append([tweet.id_str, tweet.created_at, deEmojify(tweet.full_text)])
    write_data(username, tweets_for_csv)


def get_tweets_from_hashtag(hashtag):
    for tweet in tweepy.Cursor(api.search, q='#'+hashtag, lang="en", tweet_mode="extended", since="2019-11-30").items(number_of_tweets):
        if 'retweeted_status' in tweet._json:
            continue
        else:
            tweets_for_csv.append([tweet.id_str, tweet.created_at, deEmojify(tweet.full_text)])
    write_data(hashtag, tweets_for_csv)


def write_data(searched_type, tweet_array):
    outfile = "./data/" + searched_type + "_tweets.csv"
    with open(outfile, 'w+') as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerows(tweet_array)
    file.close()


# https://stackoverflow.com/questions/33404752/removing-emojis-from-a-string-in-python
def deEmojify(inputString):
    return inputString.encode('ascii', 'ignore').decode('ascii')


# get_tweets_from_user('realDonaldTrump')
# get_tweets_from_user('HillaryClinton')
# get_tweets_from_user('JoeSestak')
# get_tweets_from_user('BorisJohnson')
# get_tweets_from_user('BarackObama')
# get_tweets_from_user('RonaldReagan')
# get_tweets_from_user('BernieSanders')
# get_tweets_from_user('Schwarzenegger')
# get_tweets_from_user('TheBushCenter')
# get_tweets_from_user('BillOReilly')
# get_tweets_from_user('marcorubio')
# get_tweets_from_user('HouseGOP')
# get_tweets_from_user('SenRickScott')
# get_tweets_from_user('SenateGOP')
# get_tweets_from_user('DevinNunes')
# get_tweets_from_user('GovMikeHuckabee')
# get_tweets_from_user('HawleyMO')
# get_tweets_from_user('RepDougCollins')
# get_tweets_from_user('RepAndyBiggsAZ')
# get_tweets_from_user('RepStefanik')
# get_tweets_from_user('RepLeeZeldin')
# get_tweets_from_user('MarshaBlackburn')
# get_tweets_from_user('DailyCaller')
# get_tweets_from_user('WayneDupreeShow')
# get_tweets_from_user('BreitbartNews')
# get_tweets_from_user('RedState')
# get_tweets_from_user('townhallcom')
# get_tweets_from_user('SolomonYue')
# get_tweets_from_user('thehill')
# get_tweets_from_user('PeteButtigieg')
# get_tweets_from_user('ChuckGrassley')
# get_tweets_from_user('JohnCornyn')
# get_tweets_from_user('GOPLeader')
# get_tweets_from_user('Jim_Jordan')
# get_tweets_from_user('RepLawrence')
# get_tweets_from_user('RudyGiuliani')
# get_tweets_from_user('VP')
# get_tweets_from_user('CNN')

# get_tweets_from_hashtag('Politics')
# get_tweets_from_hashtag('ElectionDay')
# get_tweets_from_hashtag('electiondebate')