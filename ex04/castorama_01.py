import urllib3
import certifi
import json
import time

http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())
url = 'https://sa.searchnode.net/v1/query/docs?stats=f_price_default&search_query=szlifierek+k%C4%85towych&sort=-score&limit=47&offset='
url2 = '&facet_filters.s_type=product&strict=true&query_key=A9fdhvIfC5aRN1mEsHXiAmPt4F0AQPTg'

def main():
  current_offset = 0
  max_elements = 99999
  list = []

  with open('./szlifierki_castorama.txt', 'r+') as file:
    request = http.request('GET', url + str(current_offset) + url2)
    response = json.loads(request.data)
    for x in response["docs"]:
      if x["s_title"]: list.append(x["s_title"])
    max_elements = response["doc_count"]
    current_offset += 47

    while current_offset < max_elements:
      print('check: ', current_offset, max_elements)
      time.sleep(2)
      request = http.request('GET', url + str(current_offset) + url2)
      response = json.loads(request.data)
      for x in response["docs"]:
        if x["s_title"]: list.append(x["s_title"])
      current_offset += 47

    print('list: ', len(list))
    for x in list:
      file.write(x + '\n')
    file.close()

main()
