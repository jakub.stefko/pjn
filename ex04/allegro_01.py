import requests
import re

regex = re.compile(r'title\=\"\"\>([a-zA-Z 0-9]*)[sS][zZ][lL][iI][fF][iI][eE][rR][kK][aAiI] [kK][ąĄ][tT][oO][wW][eEaA]([a-zA-Z 0-9]*)\<\/a\>')
url = 'https://allegro.pl/listing?string=szlifierka%20k%C4%85towa&bmatch=baseline-dict42-hou-1-5-1024&p='

def main():
  with open('./szlifierki_allegro.txt', 'r+') as file:
    for i in range (1, 100):
      page = requests.get(url + str(i))
      response = re.findall(regex, page.text)

      for x in response:
        file.write(x[0] + 'szlifierka kątowa' + x[1] + '\n')
    file.close()

main()
