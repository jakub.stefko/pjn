import requests
from bs4 import BeautifulSoup

url = 'https://pl.wikipedia.org/wiki/Ekstraklasa_w_piłce_nożnej_('

team_dictionary = {}

def main():
  with open('./ekstraklasa.txt', 'r+') as file:
    for year in range(2008, 2018):
      page = requests.get(url + str(year) + '/' + str(year + 1) + ')').text
    
      soup = BeautifulSoup(page, 'lxml')
      table_list = soup.find_all('table', { 'class': 'wikitable' })

      # table_number checked manually - wikipedia is inconsistent with table numers
      # i am calling their miatake, but still magic numbers are because of this
      if year < 2013:
        table_number = 1
      else:
        table_number = 2

      row_list = table_list[table_number].findAll('tr')
      row_list.pop(0)

      for row in row_list:
        cell_list = row.findAll('td')

        name = cell_list[1].find('a').string
        points = cell_list[9].find('b').string

        if name in team_dictionary:
          team_dictionary[name] += int(points)
        else:
          team_dictionary[name] = int(points)
    
    file.write(str(team_dictionary))
    file.close()

main()

